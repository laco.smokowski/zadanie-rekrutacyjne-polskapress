import Router from "./router-service.js"
import App from "./app-component.js"
import NewsService from "./news-service.js"

const app = new App('#app');
const router = new Router(app);
const service = new NewsService();

window.showMoreArticles = () => {
    app.components['newsList'].model.showAllArticles = true;
}

const newsTemplate = (news) => `
<article class='news--container'>
    <figure class='news--image'>
        <img src='${news.urlToImage}' alt='Brak Obrazu'>
    </figure>
    <figure class='news--source'>${news.author || 'Brak źródła opracowań'}</figure>
    <header class='news--title'>${news.title}</header>

</article>
`

app.addComponent({
    name: 'newsList',
    model: {
        articles: [],
        showAllArticles: false
    },
    view(model) {
        if (model.articles) {
            if (model.showAllArticles) {
                return `
                <main class='news-list--container'>
                 ${model.articles.map(news => `${newsTemplate(news)}`).join('')}
                </main>
                 `
            } else {
                return `
                <main class='news-list--container'>
                 ${model.articles.slice(0, 5).map(news => `${newsTemplate(news)}`).join('')}
                </main>
                <section>
                    <button class="button--wide--show-more" onclick='showMoreArticles()'>Zobacz więcej</button>
                </section>
                 `
            }
        }

    },
    async controller(model) {
        service.fetchNews()
            .then((response) => {
                // handle success
                const articles = response.data.articles;
                this.model.articles = articles;
            })
            .catch((error) => {
                // TODO: Show Error 
                alert('Wystąpił błąd podczas ładowania danych. Proszę spróbuj ponownie lub skonsultuj się z lekarzem lub farmaceutą')
            });
    },
})

router.addRoute('newsList', '^#/news$');
