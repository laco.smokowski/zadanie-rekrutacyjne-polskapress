class NewsService {
    constructor() {}

    fetchNews() {
        let url = 'https://newsapi.org/v2/top-headlines?' +
            'country=pl&' +
            'apiKey=f3c4ef59a3bd49ab8ee5ba06a89f7702';
        return axios.get(url);
    }
}

export default NewsService;